<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ggController extends Controller
{
    public function ganjil_genapFunc(Request $request){
        $bil_awal = $request->input('bil_awal');
        $bil_akhir = $request->input('bil_akhir');
        $result = 0;
    
        while($bil_awal <= $bil_akhir){
            if ($bil_awal % 2 == 0){
                echo "Angka " . $bil_awal . " Adalah Genap" ."<br/>";
                $bil_awal++;
            } else {
                echo "Angka " . $bil_awal . " Adalah Ganjil" ."<br/>";
                $bil_awal++;
            }
        }
        return view('welcome');
        }
    public function index(){
        return view('welcome');
    }
    
}
