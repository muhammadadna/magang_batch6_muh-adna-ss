with recursive employeeList (id, name,position, parent_id) as (
  select     ceo.id,
             ceo.nama,
             'CEO',
             ceo.atasan_id
  as Level
  from       employee ceo
  where      ceo.atasan_id IS null
  union all
  select     direktur.id,
             direktur.nama,
             'Direktur',
             direktur.atasan_id
  from       employee direktur
  inner join employeeList on direktur.atasan_id = employeeList.id + 1

  union all
  select     manager.id,
             manager.nama,
             'Manger',
             manager.atasan_id
  from       employee manager
  inner join employeeList on manager.atasan_id = employeeList.id + 2
  union all
  select     staff.id,
             staff.nama,
             'Staff',
             staff.atasan_id
  from       employee staff
  inner join employeeList on staff.atasan_id = employeeList.id + 3
)
select * from employeeList;
