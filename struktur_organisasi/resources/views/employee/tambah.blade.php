@extends('adminlte::page')

@section('title', 'Employee')

@section('content_header')
<h1>Tambah Data Employee</h1>
@stop

@section('content')

<div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Data Employee</h3>
              </div>
              <form action="/employee/store" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Enter Nama">
                  </div>
                
                  <div class="form-group">
                    <label for="exampleInputEmail1">Atasan</label>
                    <input type="text" class="form-control" name="atasan" placeholder="Enter Atasan">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Company</label>
                    <input type="text" class="form-control" name="company" placeholder="Company">
                  </div>

                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop