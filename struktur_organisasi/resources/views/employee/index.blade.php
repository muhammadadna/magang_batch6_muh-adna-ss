@extends('adminlte::page')

@section('title', 'Employee')

@section('content_header')
<h1>Table Employee</h1>
@stop

@section('content')
<a type="button" class="btn btn-block btn-primary btn-l" href="/employee/tambah">Tambah</a>

<div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Employee</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Company</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <?php $no=1; ?>
                  @foreach($employee as $data)
                  <tbody>
                  
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->nama_company }}</td>
                    <td> 
                        <a type="button" class="btn btn-warning btn-l" href="/employee/edit/{{ $data->id }}">Edit</a>
				        <a type="button" class="btn btn-danger btn-l" href="/employee/hapus/{{ $data->id }}">Hapus</a>
                    </td>
                  </tr>
                  
                  </tbody>
                  @endforeach
                </table>
              </div>
</div>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop