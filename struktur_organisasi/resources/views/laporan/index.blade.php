@extends('adminlte::page')

@section('title', 'Laporan')

@section('content_header')
<h1>Laporan</h1>
@stop

@section('content')

    <div class="container">
    <a type="button" class="btn btn-block btn-danger btn-l" href="/laporan/cetak_pdf">Export To PDF</a>
    <a type="button" class="btn btn-block btn-success btn-l" href="/laporan/tambah">Export To Excel</a>
    </div>
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Employee</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nama</th>
                    <th>Posisi</th>
                    <th>Perusahaan</th>
                  </tr>
                  </thead>
                  <?php $no=1; ?>
                  @foreach($employee as $data)
                  <tbody>
                  
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->atasan_id }}</td>
                    <td>{{ $data->nama_company }}</td>
                  </tr>
                  
                  </tbody>
                  @endforeach
                </table>
              </div>
</div>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop