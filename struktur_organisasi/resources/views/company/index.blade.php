@extends('adminlte::page')

@section('title', 'Company')

@section('content_header')
<h1>Table Company</h1>
@stop

@section('content')
<p>ISI DARI TABEL Company</p>
<a type="button" class="btn btn-block btn-primary btn-l" href="/company/tambah">Tambah</a>

<div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Company</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <? $no=1; ?>
                  @foreach($company as $data)
                  <tbody>
                  
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->nama_company }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td> 
                        <a type="button" class="btn btn-warning btn-l" href="/company/edit/{{ $data->id_company }}">Edit</a>
				        <a type="button" class="btn btn-danger btn-l" href="/company/hapus/{{ $data->id_company }}">Hapus</a>
                    </td>
                  </tr>
                  
                  </tbody>
                  @endforeach
                </table>
              </div>
</div>


@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop