@extends('adminlte::page')

@section('title', 'Company')

@section('content_header')
<h1>Edit Data Company</h1>
@stop

@section('content')

<div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Company</h3>
              </div>
              @foreach($company as $data)
              <form action="/company/update" method="POST">
              {{ csrf_field() }}
                <div class="card-body">
                <div class="form-group">
                    <input type="hidden" class="form-control" name="id" value="{{ $data->id }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $data->nama }}">
                  </div>
                
                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input type="text" class="form-control" name="alamat" value="{{ $data->alamat }}">
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              @endforeach

@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop