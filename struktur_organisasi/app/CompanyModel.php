<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model
{
    protected $table = 'company';
    protected $guarded = [];


    public $timestamps = false;

    public function employee(){

        return $this->hasMany(EmployeeModel::class, 'id_company', 'id_company');
    }
}
