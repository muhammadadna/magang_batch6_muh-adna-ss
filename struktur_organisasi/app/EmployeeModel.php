<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    protected $table = 'employee';
    protected $guarded = [];


    public $timestamps = false;

    public function company(){

        return $this->hasMany(CompanyModel::class, 'id', 'id');
    }
}
