<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanModel extends Model
{
    protected $table = 'employee, company';
    protected $guarded = [];


    public $timestamps = false;

    public function company(){

        return $this->hasMany(CompanyModel::class, 'id', 'id');
    }

    public function employee(){

        return $this->hasMany(EmployeeModel::class, 'id_company', 'id_company');
    }    
}
