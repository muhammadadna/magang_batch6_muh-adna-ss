<?php

namespace App\Http\Controllers;

use App\EmployeeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Barryvdh\DomPDF\Facade as PDF;

class LaporanController extends Controller
{
    public function index()
    {
        $company = DB::table('company')
        ->join('employee', 'employee.company_id', '=', 'company.id_company')
        ->select('employee.id', 'employee.nama','employee.atasan_id','employee.company_id','company.id_company','company.nama_company')
        ->get();
        return view('laporan/index',['employee' => $company]);
    }

    public function cetak_pdf()
    {
        
        $employee = DB::table('company')
        ->join('employee', 'employee.company_id', '=', 'company.id_company')
        ->select('employee.id', 'employee.nama','employee.atasan_id','employee.company_id','company.id_company','company.nama_company')
        ->get();

    	$pdf = PDF::loadview('/laporan/laporan_pdf',['employee'=>$employee])->setPaper('A4','potrait');
        return $pdf->stream();
    }
}
