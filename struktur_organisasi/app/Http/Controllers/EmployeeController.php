<?php

namespace App\Http\Controllers;

use App\CompanyModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\EmployeeModel;

class EmployeeController extends Controller
{
    
    public function index()
    {
        $employee = DB::table('company')
        ->join('employee', 'employee.company_id', '=', 'company.id_company')
        ->select('employee.id', 'employee.nama','employee.atasan_id','employee.company_id','company.id_company','company.nama_company')
        ->get();
        return view('employee/index',['employee' => $employee]);
    }


    public function tambah()
    {
        $company = CompanyModel::all();

        return view('/employee/tambah');
    
    }

    public function store(Request $request)
    {
        $employee = EmployeeModel::list('nama_company', 'id_company');

        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'nama_company' => $request->company
        ]);

        return redirect('/employee');
    
    }

    public function edit($id)
    {

        $employee = DB::table('employee')->where('id',$id)->get();

        return view('/employee/edit',['employee' => $employee]);
    }

    public function update(Request $request)
    {

        DB::table('employee')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'company_id' => $request->company
        ]);

        return redirect('/employee');
    }

    public function hapus($id)
    {

        DB::table('employee')->where('id',$id)->delete();
            

        return redirect('/employee');
    }

}
