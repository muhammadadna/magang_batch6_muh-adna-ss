<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class vokalController extends Controller
{
    public function index()
    {
        return view('hitungvokal');
    }
    public function aksi(Request $request)
    {
        $kata= $request->kata;
        $data = array();
        $hit = 0;
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        for ($i = 0; $i < strlen($kata); $i++) {
            if ($kata[$i] == 'a') {
                if ($a == 0) {
                    $data[$hit] = $kata[$i];
                    $hit++;
                    $a = 1;
                }
            } else if ($kata[$i] == 'i') {
                if ($b == 0) {
                    $data[$hit] = $kata[$i];
                    $hit++;
                    $b = 1;
                }
            } else if ($kata[$i] == 'u') {
                if ($c == 0) {
                    $data[$hit] = $kata[$i];
                    $hit++;
                    $c = 1;
                }
            } else if ($kata[$i] == 'e') {
                if ($d == 0) {
                    $data[$hit] = $kata[$i];
                    $hit++;
                    $d = 1;
                }
            } else if ($kata[$i] == 'o') {
                if ($e == 0) {
                    $data[$hit] = $kata[$i];
                    $hit++;
                    $e = 1;
                }
            }
        }
        return view('hitungvokal', compact('kata','hit','data'));
    }
}
