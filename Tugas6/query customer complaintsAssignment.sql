SELECT MONTH(C1) AS bulan, COUNT(*) AS Jumlah_BULAN
FROM ConsumerComplaints
GROUP BY MONTH(C1);

SELECT * FROM ConsumerComplaints WHERE C11 LIKE 'Older American';

SELECT C8 AS Company, C15 AS Closed
FROM ConsumerComplaints
WHERE C15 = 'Closed';

SELECT C8, C15, COUNT(*) FROM ConsumerComplaints GROUP BY C8 order by count(*) DESC;

SELECT C8 AS Company,
(SELECT COUNT(c.C15) FROM ConsumerComplaints c WHERE c.C15 = 'Closed') as Closed,
(SELECT COUNT(c.C15) FROM ConsumerComplaints c WHERE c.C15 ='Closed with explanation') as Closed_with_explanation,
(SELECT COUNT(c.C15) FROM ConsumerComplaints c WHERE c.C15 ='Closed with non-monetary relief') as Closed_with_non_monetary_relief
FROM ConsumerComplaints c;

SELECT C8 as Company,
(SELECT COUNT(p.C11) FROM ConsumerComplaints p WHERE p.C11 = 'Older American') as total_kabupaten_kota
FROM ConsumerComplaints p