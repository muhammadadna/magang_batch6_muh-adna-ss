<!DOCTYPE html>
<html>
<head>
	<title>CRUD Struktur Organisasi</title>
</head>
<body>
	<h3>Edit Employee</h3>

	@foreach($employee as $p)
	<form action="/employee/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->id }}">
		Nama<br/> <input type="text" required="required" name="nama" value="{{ $p->nama }}"> <br/>
		Atasan Id <br/><input type="number" required="required" name="atasan" value="{{ $p->atasan_id }}"> <br/>
		Company Id<br/> <input type="number" required="required" name="company" value="{{ $p->company_id }}"> <br/><br/>
		<input type="submit" value="Simpan Data">
	</form>
    <br/>
    <a href="/employee"><button>Kembali</button></a>
	@endforeach
		
</body>
</html>