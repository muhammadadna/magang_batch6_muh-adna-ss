<!DOCTYPE html>
<html>
<head>
	<title>CRUD Struktur Organisasi</title>
</head>
<body>
	<h3>Data Employee</h3>


	<a href="/employee/tambah"><button>Tambah Employee Baru</button></a>
	
	<br/>
	<br/>

	<table border="3">
		<tr>
			<th>Nama</th>
			<th>Atasan</th>
			<th>Company</th>
			<th>Opsi</th>
		</tr>
		@foreach($employee as $p)
		<tr>
			<td>{{ $p->nama }}</td>
			<td>{{ $p->atasan_id }}</td>
			<td>{{ $p->company_id }}</td>
			<td>
				<a href="/employee/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/employee/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>

</body>
</html>