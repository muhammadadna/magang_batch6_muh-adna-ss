<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index()
    {
    	$company = DB::table('company')->get();
 
    	return view('company/index',['company' => $company]);
 
    }

    public function tambah()
    {

        return view('company/tambah');
    
    }

    public function store(Request $request)
    {

        DB::table('company')->insert([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect('/company');
    
    }

    public function edit($id)
    {

        $company = DB::table('company')->where('id',$id)->get();

        return view('company/edit',['company' => $company]);
    }

    public function update(Request $request)
    {

        DB::table('company')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect('/company');
    }

    public function hapus($id)
    {

        DB::table('company')->where('id',$id)->delete();
            

        return redirect('/company');
    }
}
