<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function index()
    {
    	$employee = DB::table('employee')->get();
 
    	return view('index',['employee' => $employee]);
 
    }

    public function tambah()
    {

        return view('tambah');
    
    }

    public function store(Request $request)
    {

        DB::table('employee')->insert([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'company_id' => $request->company
        ]);

        return redirect('/employee');
    
    }

    public function edit($id)
    {

        $employee = DB::table('employee')->where('id',$id)->get();

        return view('edit',['employee' => $employee]);
    }

    public function update(Request $request)
    {

        DB::table('employee')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'atasan_id' => $request->atasan,
            'company_id' => $request->company
        ]);

        return redirect('/employee');
    }

    public function hapus($id)
    {

        DB::table('employee')->where('id',$id)->delete();
            

        return redirect('/employee');
    }

}
