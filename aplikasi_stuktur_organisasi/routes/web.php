<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/employee','EmployeeController@index');
Route::get('/employee/tambah','EmployeeController@tambah');
Route::post('/employee/store','EmployeeController@store');
Route::get('/employee/edit/{id}','EmployeeController@edit');
Route::post('/employee/update','EmployeeController@update');
Route::get('/employee/hapus/{id}','EmployeeController@hapus');


Route::get('/company','CompanyController@index');
Route::get('/company/tambah','CompanyController@tambah');
Route::post('/company/store','CompanyController@store');
Route::get('/company/edit/{id}','CompanyController@edit');
Route::post('/company/update','CompanyController@update');
Route::get('/company/hapus/{id}','CompanyController@hapus');